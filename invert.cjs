let invertObject = {};

function invert(obj) {

    for (let key in obj){
        invertObject[obj[key]] = key;
    }

    return invertObject

}

module.exports = invert;
