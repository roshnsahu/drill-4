let output = [];

function pairs(obj) {

    for (let key in obj){
        output.push([key, obj[key]]);
    }

    return output
}

module.exports = pairs;